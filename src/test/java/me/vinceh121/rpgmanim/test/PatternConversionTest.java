package me.vinceh121.rpgmanim.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import me.vinceh121.rpgmanim.ConvertUtils;

class PatternConversionTest {

	@ParameterizedTest
	@CsvSource({ "a,100", "b,101", "c,102", "z,125", "A,0", "B,1", "C,2", "Z,25" })
	void patternToLetter(final char expected, final int pattern) {
		Assertions.assertEquals(expected, ConvertUtils.patternToLetter(pattern));
	}

	@ParameterizedTest
	@CsvSource({ "a,100", "b,101", "c,102", "z,125", "A,0", "B,1", "C,2", "Z,25" })
	void letterToPattern(final char letter, final int patternExpected) {
		Assertions.assertEquals(patternExpected, ConvertUtils.letterToPattern(letter));
	}
}
