package me.vinceh121.rpgmanim.test;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;

import me.vinceh121.rpgmanim.editor.KerningConfigurator;

class KerningTest {
	@Test
	void testCrop() throws IOException {
		BufferedImage img = ImageIO.read(KerningTest.class.getClassLoader().getResourceAsStream("a.png"));

		System.out.println(KerningConfigurator.autoCrop(img));
	}
}
