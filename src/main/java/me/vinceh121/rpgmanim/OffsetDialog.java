package me.vinceh121.rpgmanim;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;

import me.vinceh121.rpgmanim.entities.Animation;
import me.vinceh121.rpgmanim.entities.Frame;

public class OffsetDialog extends JDialog {
	private static final long serialVersionUID = -2989469410881884218L;
	private static final double OFFSET = 5;
	private Runnable onUpdate = () -> {};

	public OffsetDialog(final Animation anim) {
		this.setLayout(new FlowLayout());

		final JButton btnLeft = new JButton("←");
		btnLeft.addActionListener(e -> {
			anim.getFrames().stream().flatMap(Frame::stream).forEach(c -> {
				c.setX(c.getX() - OFFSET);
			});

			this.onUpdate.run();
		});
		this.add(btnLeft);

		final JButton btnRight = new JButton("→");
		btnRight.addActionListener(e -> {
			anim.getFrames().stream().flatMap(Frame::stream).forEach(c -> {
				c.setX(c.getX() + OFFSET);
			});

			this.onUpdate.run();
		});
		this.add(btnRight);

		final JButton btnDown = new JButton("↓");
		btnDown.addActionListener(e -> {
			anim.getFrames().stream().flatMap(Frame::stream).forEach(c -> {
				c.setY(c.getY() + OFFSET);
			});

			this.onUpdate.run();
		});
		this.add(btnDown);

		final JButton btnUp = new JButton("↑");
		btnUp.addActionListener(e -> {
			anim.getFrames().stream().flatMap(Frame::stream).forEach(c -> {
				c.setY(c.getY() - OFFSET);
			});

			this.onUpdate.run();
		});
		this.add(btnUp);

		this.pack();
	}

	public void setOnUpdate(final Runnable onUpdate) {
		this.onUpdate = onUpdate;
	}
}
