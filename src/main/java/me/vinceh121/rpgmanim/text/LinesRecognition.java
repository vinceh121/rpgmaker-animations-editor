package me.vinceh121.rpgmanim.text;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;

import me.vinceh121.rpgmanim.ConvertUtils;
import me.vinceh121.rpgmanim.entities.Cell;
import me.vinceh121.rpgmanim.entities.Frame;

/**
 * Discriminates lines by same Y value for letters
 *
 * XXX Maybe also assume consistent X offset with previous letter?
 * Hindsight on the previous note: no
 */
public class LinesRecognition {
	private static final Logger LOG = LogManager.getLogger(LinesRecognition.class);
	private final Frame frame;

	public LinesRecognition(final Frame frame) {
		this.frame = frame;
	}

	public List<Line> calculateLines() {
		// Keys: Y value
		final Map<Double, IncompleteLine> map = new Hashtable<>();

		for (int i = 0; i < this.frame.size(); i++) {
			final Cell c = this.frame.get(i);
			final char letter;

			try {
				letter = ConvertUtils.patternToLetter((int) c.getPattern());
			} catch (final IllegalArgumentException e) {
				LOG.warn(new FormattedMessage("Failed to get letter from pattern of cell {}", i), e);
				continue;
			}

			if (!map.containsKey(c.getY())) {
				map.put(c.getY(), new IncompleteLine(c.getX(), c.getScale() / 100f, Integer.MAX_VALUE));
			}

			final IncompleteLine il = map.get(c.getY());
			final StringBuilder sb = il.sb;
			sb.append(letter);

			if (i > 0) {
				il.width = c.getX() - this.frame.get(i - 1).getX();
			}
			if (il.minX > c.getX()) {
				il.minX = c.getX();
			}

			if (il.scale * 100 > c.getScale()) {
				il.scale = c.getScale() / 100f;
			}
		}

		final List<Line> lines = new ArrayList<>();

		for (final Double y : map.keySet()) {
			final IncompleteLine il = map.get(y);
			final Line l = new Line();
			l.setText(il.sb.toString());
			l.setScale(il.scale);
			l.setLetterWidth(il.width);
			l.setX(il.minX);
			l.setY(y);
			lines.add(l);
		}

		return lines;
	}

	private class IncompleteLine {
		private double minX, width, scale;
		private final StringBuilder sb = new StringBuilder();

		private IncompleteLine(final double minX, final double scale, final double width) {
			this.minX = minX;
			this.scale = scale;
			this.width = width;
		}
	}
}
