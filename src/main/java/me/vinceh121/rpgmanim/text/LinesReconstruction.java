package me.vinceh121.rpgmanim.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.vinceh121.rpgmanim.ConvertUtils;
import me.vinceh121.rpgmanim.entities.Cell;
import me.vinceh121.rpgmanim.entities.Frame;
import me.vinceh121.rpgmanim.entities.Kerning;

public class LinesReconstruction {
	private final List<Line> lines;
	private final Map<Character, Kerning> kernings = new HashMap<>();
	private Frame baseFrame;
	private List<Frame> frames;
	private int globalKerning;

	public LinesReconstruction(final List<Line> lines) {
		this.lines = lines;
	}

	public void calculateBaseFrame() {
		this.baseFrame = new Frame();
		for (final Line line : this.lines) {
			this.baseFrame.addAll(this.makeLine(line));
		}
	}

	private List<Cell> makeLine(final Line line) {
		final List<Cell> cells = new ArrayList<>();

		double offset = line.getX();

		for (int i = 0; i < line.getText().length(); i++) {
			final char c = line.getText().charAt(i);

			if (c == ' ') {
				offset += line.getLetterWidth();
				continue;
			}

			final Kerning kern = this.kernings.get(c);

			final Cell cell = new Cell(true);
			cell.setX(offset - kern.getyStart());
			offset += ((kern.getyEnd() - kern.getyStart()) + this.globalKerning) * line.getScale();
			cell.setY(line.getY());
			cell.setOpacity(255);
			cell.setPattern(ConvertUtils.letterToPattern(c));
			cell.setScale((int) (line.getScale() * 100));
			cells.add(cell);
		}
		return cells;
	}

	public void extendBaseFrame(final int frameCount) {
		this.frames = new ArrayList<>(frameCount);
		for (int i = 0; i < frameCount; i++) {
			this.frames.add(LinesReconstruction.deepCopy(this.baseFrame));
		}
	}

	public static final double DEFAULT_FADE_ACCENT = 2;

	/**
	 *
	 * @param start     first frame affected by the fade effect (inclusive)
	 * @param end       last frame affected by the fade effect (exclusive)
	 * @param direction 1 for fade in, -1 for fade out
	 */
	public void calculateFade(final int start, final int end, final int direction) {
		if (direction != -1 && direction != 1) {
			throw new IllegalArgumentException("Invalid direction");
		}

		for (int i = start; i <= end; i++) {
			final Frame frame = this.frames.get(i);
			Collections.sort(frame, (c1, c2) -> Double.compare(c1.getX(), c2.getX()));

			for (int j = 0; j < frame.size(); j++) {
				final Cell cell = frame.get(j);
				final float frameScaledFloat = direction == 1 ? map(start, end, 0, frame.size(), i)
						: map(start, end, frame.size(), -frame.size(), i);
				final int frameScaled = (int) Math.ceil(frameScaledFloat);
				final double max = f(j, direction, frame.size());
				double y = f(j, direction, (int) frameScaled);

				if (y < 0) {
					y = 0;
				}

				cell.setOpacity((int) ((y / max) * 255));
			}
		}
	}

	public static float map(float inRangeStart, float inRangeEnd, float outRangeStart, float outRangeEnd, float value) {
		return outRangeStart + (value - inRangeStart) * (outRangeEnd - outRangeStart) / (inRangeEnd - inRangeStart);
	}

	private static int f(int x, int direction, int frame) {
		return -direction * x + frame;
	}

	public static Frame deepCopy(final Frame src) {
		final Frame dest = new Frame();
		for (final Cell e : src) {
			dest.add(new Cell(e));
		}
		return dest;
	}

	public Frame getBaseFrame() {
		return this.baseFrame;
	}

	public List<Frame> getFrames() {
		return this.frames;
	}

	public Map<Character, Kerning> getKernings() {
		return this.kernings;
	}

	public int getGlobalKerning() {
		return globalKerning;
	}

	public void setGlobalKerning(int globalKerning) {
		this.globalKerning = globalKerning;
	}

	public static double averageLineHeight(final List<Line> lines) {
		if (lines.size() <= 1) {
			return ConvertUtils.CELL_PIXEL_SIZE;
		}

		double sum = 0;

		for (int i = 1; i < lines.size(); i++) {
			sum += lines.get(i).getY() - lines.get(i - 1).getY();
		}

		return sum / (double) lines.size();
	}
}
