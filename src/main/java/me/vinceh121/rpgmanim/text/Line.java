package me.vinceh121.rpgmanim.text;

public class Line {
	private double scale = 1, x, y, letterWidth;
	private String text;

	public double getX() {
		return this.x;
	}

	public void setX(final double x) {
		this.x = x;
	}

	public double getY() {
		return this.y;
	}

	public void setY(final double y) {
		this.y = y;
	}

	/**
	 * @return The scale between 0 and 1 inclusive
	 */
	public double getScale() {
		return this.scale;
	}

	/**
	 * @param scale The scale between 0 and 1 inclusive
	 */
	public void setScale(final double scale) {
		this.scale = scale;
	}

	public double getLetterWidth() {
		return this.letterWidth;
	}

	public void setLetterWidth(final double letterWidth) {
		this.letterWidth = letterWidth;
	}

	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Line \""
				+ text
				+ "\" [scale="
				+ scale
				+ ", x="
				+ x
				+ ", y="
				+ y
				+ ", letterWidth="
				+ letterWidth
				+ "]";
	}
}
