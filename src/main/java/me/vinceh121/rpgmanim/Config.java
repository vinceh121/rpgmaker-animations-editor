package me.vinceh121.rpgmanim;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import me.vinceh121.rpgmanim.entities.Kerning;

public class Config {
	private String animations, charset, assetsRoot;

	@JsonInclude(content = Include.NON_EMPTY)
	private final Map<String, String> images = new HashMap<>();

	@JsonInclude(content = Include.NON_EMPTY)
	private final Map<String, String> audios = new HashMap<>();

	@JsonInclude(content = Include.NON_EMPTY)
	private final Map<Character, Kerning> kernings = new HashMap<>();
	private int globalKerning;

	public String getAnimations() {
		return this.animations;
	}

	public void setAnimations(final String animations) {
		this.animations = animations;
	}

	public String getCharset() {
		return this.charset;
	}

	public void setCharset(final String charset) {
		this.charset = charset;
	}

	public Map<String, String> getImages() {
		return this.images;
	}

	public Map<String, String> getAudios() {
		return this.audios;
	}

	public Map<Character, Kerning> getKernings() {
		return this.kernings;
	}

	public String getAssetsRoot() {
		return assetsRoot;
	}

	public void setAssetsRoot(String assetsRoot) {
		this.assetsRoot = assetsRoot;
	}

	public int getGlobalKerning() {
		return globalKerning;
	}

	public void setGlobalKerning(int globalKerning) {
		this.globalKerning = globalKerning;
	}
}
