package me.vinceh121.rpgmanim;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComponent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFormatMessage;

import com.jhlabs.image.OpacityFilter;

import me.vinceh121.rpgmanim.entities.Animation;
import me.vinceh121.rpgmanim.entities.Cell;
import me.vinceh121.rpgmanim.entities.Frame;

public class FrameEditComponent extends JComponent implements MouseListener, MouseMotionListener {
	private static final Logger LOG = LogManager.getLogger(FrameEditComponent.class);
	private static final long serialVersionUID = -2180294763907282000L;
	public static final int CANVAS_WIDTH = 816, CANVAS_HEIGHT = 624;
	private final Set<Cell> selectedCells = new HashSet<>();
	private int animId, frameId, dragStartX = 0, dragStartY = 0;
	private Animation animation;
	private Frame frame;
	private BufferedImage img1, img2;
	private boolean renderOpacity = true, fuzzyDrag = true;
	private double scale = 1;
	private String tooltip;

	public FrameEditComponent() {
		this.setBackground(Color.BLACK);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	@Override
	protected void paintComponent(final Graphics g) {
		g.setColor(this.getBackground());
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		if (this.frame == null) {
			return;
		}

		g.translate(CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2);

		for (final Cell c : this.frame) {
			if (c.getPattern() >= 100 && this.img2 != null) {
				this.drawCell(g, c, this.img2);
			} else if (c.getPattern() >= 0 && this.img1 != null) {
				this.drawCell(g, c, this.img1);
			}
		}

		// draw 0 point
		g.setColor(Color.RED);
		g.drawLine(0, 10, 0, -10);
		g.drawLine(10, 0, -10, 0);

		// draw border
		g.drawLine(-CANVAS_WIDTH / 2, -CANVAS_HEIGHT / 2, -CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2);
		g.drawLine(-CANVAS_WIDTH / 2, -CANVAS_HEIGHT / 2, CANVAS_WIDTH / 2, -CANVAS_HEIGHT / 2);
		g.drawLine(CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2, CANVAS_WIDTH / 2, -CANVAS_HEIGHT / 2);
		g.drawLine(CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2, -CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2);

		// tooltip
		g.setColor(Color.WHITE);

		if (this.tooltip != null) {
			g.drawString(this.tooltip, -CANVAS_WIDTH / 2 + 16, CANVAS_HEIGHT / 2 + 16);
		}
	}

	private void drawCell(final Graphics g, final Cell c, final BufferedImage img) {
		try {
			final Point p = ConvertUtils.getCoords((int) c.getPattern());
			final BufferedImage cellImg = img.getSubimage(p.x, p.y, 192, 192);

			double scaleX = (double) c.getScale() / 100D * this.scale;
			if (c.get(5) > 0) {
				scaleX *= -1;
			}
			final double scaleY = (double) c.getScale() / 100D * this.scale;

			final int width = (int) (192 * scaleX);
			final int height = (int) (192 * scaleY);

			final BufferedImage cellImgPost;
			if (this.renderOpacity && cellImg.getAlphaRaster() != null) {
				final OpacityFilter filter = new OpacityFilter((int) c.getOpacity());
				cellImgPost = filter.filter(cellImg, null);
			} else {
				cellImgPost = cellImg;
			}

			final int x = (int) ((c.getX() - (192f * 1f / 3f)) * this.scale); // XXX this should be 1/2 so why does 1/3 work?
			final int y = (int) ((c.getY() - (192f * 1f / 3f)) * this.scale);

			g.drawImage(cellImgPost, x, y, width, height, this);

			if (this.selectedCells.contains(c)) {
				g.setColor(Color.RED);
				g.drawRect(x, y, width, height);
			}
		} catch (final Exception e) {
			LOG.error(new MessageFormatMessage("Failed to render cell: {0}", c), e);
		}
	}

	public Cell getCellAt(int x, int y) {
		if (this.frame == null) {
			return null;
		}

		for (final Cell c : this.frame) {
			final Rectangle r = new Rectangle((int) c.getX() + CANVAS_WIDTH / 2 - (192 / 2), (int) c.getY() + CANVAS_HEIGHT / 2 - (192 / 2),
					(int) (192 * (c.getScale() / 100)), (int) (192 * (c.getScale() / 100)));

			if (r.contains(x, y)) {
				return c;
			}
		}

		return null;
	}

	public void clearSelection() {
		this.selectedCells.clear();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		final Cell c = this.getCellAt(e.getX(), e.getY());

		if (!e.isShiftDown()) {
			this.clearSelection();
		}

		if (c != null) {
			this.selectedCells.add(c);
		}

		this.repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		for (final Cell c : this.selectedCells) {
			if (this.fuzzyDrag) {
				this.animation.getFrames()
						.stream()
						.flatMap(f -> f.stream())
						.filter(cell -> cell.fuzzyEquals(c))
						.forEach(cell -> {
							final double x = this.dragStartX + e.getX() - cell.getX();
							final double y = this.dragStartY + e.getY() - cell.getY();

							cell.setX(x);
							cell.setY(y);
						});
			} else {
				c.setX(e.getX());
				c.setY(e.getY());
			}
		}

		this.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		final Cell c = this.getCellAt(e.getX(), e.getY());

		if (c != null) {
			this.tooltip = c.toString();

			this.repaint();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.dragStartX = e.getX();
		this.dragStartY = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	public Frame getFrame() {
		return this.frame;
	}

	public void setFrame(final Frame frame) {
		this.frame = frame;
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

	public BufferedImage getImg1() {
		return this.img1;
	}

	public void setImg1(final BufferedImage img1) {
		this.img1 = img1;
	}

	public BufferedImage getImg2() {
		return this.img2;
	}

	public void setImg2(final BufferedImage img2) {
		this.img2 = img2;
	}

	public int getAnimId() {
		return this.animId;
	}

	public void setAnimId(final int animId) {
		this.animId = animId;
	}

	public int getFrameId() {
		return this.frameId;
	}

	public void setFrameId(final int frameId) {
		this.frameId = frameId;
	}

	public boolean isRenderOpacity() {
		return this.renderOpacity;
	}

	public void setRenderOpacity(final boolean renderOpacity) {
		this.renderOpacity = renderOpacity;
	}

	public double getScale() {
		return this.scale;
	}

	public void setScale(final double scale) {
		this.scale = scale;
	}
}
