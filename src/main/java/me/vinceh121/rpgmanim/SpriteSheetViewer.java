package me.vinceh121.rpgmanim;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class SpriteSheetViewer extends JFrame {
	private static final long serialVersionUID = 1L;
	private final BufferedImage image;

	public SpriteSheetViewer(BufferedImage image) throws HeadlessException {
		this.image = image;

		this.setSize(image.getWidth(), image.getHeight());

		this.paintLines(image.createGraphics());

		final JLabel lbl = new JLabel(new ImageIcon(image));
		this.add(lbl);

		this.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_S && e.isControlDown()) {
					JFileChooser fc = new JFileChooser();
					int option = fc.showSaveDialog(null);

					if (option == JFileChooser.APPROVE_OPTION) {
						try {
							ImageIO.write(image, "png", fc.getSelectedFile());
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}

	private void paintLines(Graphics2D g) {
		g.drawImage(image, 0, 0, this);
		g.setColor(Color.RED);

		for (int i = 0; i < 100; i++) {
			final Point coords = ConvertUtils.getCoords(i);
			g.drawRect(coords.x, coords.y, ConvertUtils.CELL_PIXEL_SIZE, ConvertUtils.CELL_PIXEL_SIZE);
		}
	}
}
