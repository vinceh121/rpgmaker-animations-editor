package me.vinceh121.rpgmanim.editor;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import me.vinceh121.rpgmanim.Config;
import me.vinceh121.rpgmanim.ConvertUtils;
import me.vinceh121.rpgmanim.cache.TextureCache;
import me.vinceh121.rpgmanim.entities.Kerning;

public class KerningConfigurator extends JDialog {
	private static final long serialVersionUID = -1092655060059568713L;
	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdeéfghijklmnopqrstuvwxyz";
	private final Config config;

	public KerningConfigurator(Config config) {
		this.config = config;

		try {
			setDefaultKernings(this.config.getKernings()); // FIXME bad place to have this here
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.setTitle("Kerning config");
		this.setSize(500, 500);
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		this.setLayout(new BorderLayout());

		JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		KerningPreview preview = new KerningPreview(this.config.getKernings());
		preview.setText(ALPHABET);

		split.setBottomComponent(new JScrollPane(preview));

		JPanel pnlOffsets = new JPanel();
		pnlOffsets.setLayout(new BoxLayout(pnlOffsets, BoxLayout.LINE_AXIS));
		split.setTopComponent(new JScrollPane(pnlOffsets));

		JTextField txtSample = new JTextField(ALPHABET);
		txtSample.addActionListener(e -> {
			preview.setText(txtSample.getText());

			preview.repaint();
		});
		pnlOffsets.add(txtSample);

		JPanel pnlGlobalKern = new JPanel();
		JLabel lblGlobalKern = new JLabel("<html><h3>Global kerning</h3></html>");
		pnlGlobalKern.add(lblGlobalKern);

		JSpinner spinGlobalKern = new JSpinner();
		spinGlobalKern.addChangeListener(e -> {
			config.setGlobalKerning((int) spinGlobalKern.getValue());
			preview.setGlobalKerning((int) spinGlobalKern.getValue());

			preview.repaint();
		});
		pnlGlobalKern.add(spinGlobalKern);

		pnlOffsets.add(pnlGlobalKern);

		for (int i = 0; i < ALPHABET.length(); i++) {
			final char ch = ALPHABET.charAt(i);
			Kerning origKern = this.config.getKernings().get(ch);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridy = i;

			JPanel pnlOff = new JPanel();

			JLabel lblLetter = new JLabel("<html><h3>" + Character.toString(ch) + "</h3></html>");
			pnlOff.add(lblLetter);

			JSpinner spinStart = new JSpinner();
			spinStart.setValue(origKern.getyStart());
			spinStart.addChangeListener(e -> {
				Kerning kern = this.config.getKernings().get(ch);
				kern.setyStart((int) spinStart.getValue());

				preview.repaint();
			});

			pnlOff.add(spinStart);

			JSpinner spinEnd = new JSpinner();
			spinEnd.setValue(origKern.getyEnd());
			spinEnd.addChangeListener(e -> {
				Kerning kern = this.config.getKernings().get(ch);
				kern.setyEnd((int) spinEnd.getValue());

				preview.repaint();
			});

			pnlOff.add(spinEnd);

			pnlOffsets.add(pnlOff);
		}

		this.add(split, BorderLayout.CENTER);
	}

	private static void setDefaultKernings(Map<Character, Kerning> kernings) throws IOException {
		for (char ch : ALPHABET.toCharArray()) {
			if (!kernings.containsKey(ch)) {
				String sheetName = Character.isUpperCase(ch) ? "Capital" : "Lowercase";
				BufferedImage sheet = TextureCache.getInstance().getCached(sheetName);

				Point p = ConvertUtils.getCoords(ConvertUtils.letterToPattern(ch));
				BufferedImage cellImg = sheet.getSubimage(p.x, p.y, 192, 192);

				kernings.put(ch, autoCrop(cellImg));
			}
		}
	}

	public static Kerning autoCrop(BufferedImage img) {
		Kerning kern = new Kerning(0, img.getWidth() - 1);

		BufferedImage stripe = img.getSubimage(0, 0, 1, img.getHeight());

		while (isAllEqual(stripe)) {
			kern.setyStart(kern.getyStart() + 1);

			stripe = img.getSubimage(kern.getyStart(), 0, 1, img.getHeight());
		}

		stripe = img.getSubimage(kern.getyEnd(), 0, 1, img.getHeight());

		while (isAllEqual(stripe)) {
			kern.setyEnd(kern.getyEnd() - 1);

			stripe = img.getSubimage(kern.getyEnd(), 0, 1, img.getHeight());
		}

		return kern;
	}

	private static boolean isAllEqual(BufferedImage img) {
		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				int[] alpha = img.getAlphaRaster().getPixel(x, y, (int[]) null);

				if (alpha[0] != 0) {
					return false;
				}
			}
		}

		return true;
	}
}
