package me.vinceh121.rpgmanim.editor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;

import me.vinceh121.rpgmanim.Config;
import me.vinceh121.rpgmanim.entities.Animation;
import me.vinceh121.rpgmanim.entities.Frame;
import me.vinceh121.rpgmanim.text.Line;
import me.vinceh121.rpgmanim.text.LinesRecognition;
import me.vinceh121.rpgmanim.text.LinesReconstruction;

public class TextEditor extends JFrame {
	private static final long serialVersionUID = -5197919029634369309L;
	private final JTextArea text;
	private final Animation animation;
	private final Config config;
	private List<Line> lines;
	private Consumer<LinesReconstruction> callback;

	public TextEditor(final Animation animation, final Config config) {
		this.animation = animation;
		this.config = config;

		this.setTitle("Text Animation Editor: " + animation.getName());
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setSize(400, 500);
		this.setLayout(new BorderLayout());

		this.text = new JTextArea();
		this.add(this.text, BorderLayout.CENTER);

		final JPanel panelFades = new JPanel(new GridBagLayout());

		final GridBagConstraints gbcFadeInStartSpinner = new GridBagConstraints();
		gbcFadeInStartSpinner.gridx = 1;
		gbcFadeInStartSpinner.gridy = 0;

		final JSpinner fadeInStartSpinner
				= new JSpinner(new SpinnerNumberModel(0, 0, animation.getFrames().size() - 1, 1));
		panelFades.add(fadeInStartSpinner, gbcFadeInStartSpinner);

		final GridBagConstraints gbcFadeInEndSpinner = new GridBagConstraints();
		gbcFadeInEndSpinner.gridx = 2;
		gbcFadeInEndSpinner.gridy = 0;

		final JSpinner fadeInEndSpinner
				= new JSpinner(new SpinnerNumberModel(0, 0, animation.getFrames().size() - 1, 1));
		panelFades.add(fadeInEndSpinner, gbcFadeInEndSpinner);

		final GridBagConstraints gbcLblFadeIn = new GridBagConstraints();
		gbcLblFadeIn.gridx = 0;
		gbcLblFadeIn.gridy = 0;
		panelFades.add(new JLabel("Fade in"), gbcLblFadeIn);

		final GridBagConstraints gbcFadeOutStartSpinner = new GridBagConstraints();
		gbcFadeOutStartSpinner.gridx = 1;
		gbcFadeOutStartSpinner.gridy = 1;

		final JSpinner fadeOutStartSpinner
				= new JSpinner(new SpinnerNumberModel(0, 0, animation.getFrames().size() - 1, 1));
		panelFades.add(fadeOutStartSpinner, gbcFadeOutStartSpinner);

		final GridBagConstraints gbcFadeOutEndSpinner = new GridBagConstraints();
		gbcFadeOutEndSpinner.gridx = 2;
		gbcFadeOutEndSpinner.gridy = 1;

		final JSpinner fadeOutEndSpinner
				= new JSpinner(new SpinnerNumberModel(0, 0, animation.getFrames().size() - 1, 1));
		panelFades.add(fadeOutEndSpinner, gbcFadeOutEndSpinner);

		final GridBagConstraints gbcLblFadeOut = new GridBagConstraints();
		gbcLblFadeOut.gridx = 0;
		gbcLblFadeOut.gridy = 1;
		panelFades.add(new JLabel("Fade out"), gbcLblFadeOut);

		this.add(panelFades, BorderLayout.WEST);

		final JPanel panelButtons = new JPanel(new FlowLayout());
		this.add(panelButtons, BorderLayout.SOUTH);

		final JButton btnOk = new JButton("Fuck yea, edit this mfing text!");
		btnOk.addActionListener(e -> {
			try {
				final int fadeInStart = (int) fadeInStartSpinner.getValue(),
						fadeInEnd = (int) fadeInEndSpinner.getValue(),
						fadeOutStart = (int) fadeOutStartSpinner.getValue(),
						fadeOutEnd = (int) fadeOutEndSpinner.getValue();

				if (fadeInStart > fadeInEnd) {
					JOptionPane.showMessageDialog(null, "Fade in bounds are backwards");
					return;
				}

				if (fadeOutStart > fadeOutEnd) {
					JOptionPane.showMessageDialog(null, "Fade out bounds are backwards");
					return;
				}

				final String[] editedLines = this.text.getText().split("\n");
				for (int i = 0; i < this.lines.size(); i++) {
					this.lines.get(i).setText(editedLines[i]);
				}

				if (editedLines.length > this.lines.size()) {
					final double lastX = this.lines.get(this.lines.size() - 1).getX();
					final double lastY = this.lines.get(this.lines.size() - 1).getY();
					final double lineHeight = LinesReconstruction.averageLineHeight(this.lines);
					
					for (int i = this.lines.size(); i < editedLines.length; i++) {
						final Line newLine = new Line();
						newLine.setText(editedLines[i]);
						newLine.setY(lastY + (this.lines.size() - i + 1) * lineHeight);
						newLine.setX(lastX);
						this.lines.add(newLine);
					}
				}

				final LinesReconstruction lr = new LinesReconstruction(this.lines);
				lr.setGlobalKerning(this.config.getGlobalKerning());
				lr.getKernings().putAll(this.config.getKernings());
				lr.calculateBaseFrame();
				lr.extendBaseFrame(this.animation.getFrames().size());
				if (!fadeInStartSpinner.getValue().equals(fadeInEndSpinner.getValue())) {
					lr.calculateFade(fadeInStart, fadeInEnd, 1);
				}
				if (!fadeOutStartSpinner.getValue().equals(fadeOutEndSpinner.getValue())) {
					lr.calculateFade(fadeOutStart, fadeOutEnd, -1);
				}
				this.dispose();
				this.callback.accept(lr);
			} catch (final Exception ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null,
						"Oopsie poopsie we have made a smol dookie 💩\nHere's what fucked up: " + ex);
			}
		});
		panelButtons.add(btnOk);

		final JButton btnCancel = new JButton("Fuck off");
		btnCancel.addActionListener(e -> this.dispose());
		panelButtons.add(btnCancel);
		this.recognizeText(this.animation.getFrames().get(0));
	}

	private void recognizeText(final Frame frame) {
		final LinesRecognition lr = new LinesRecognition(frame);
		final StringBuilder sb = new StringBuilder();
		this.lines = lr.calculateLines();
		for (final Line l : this.lines) {
			sb.append(l.getText());
			sb.append("\n");
		}
		sb.deleteCharAt(sb.length() - 1);
		this.text.setText(sb.toString());
	}

	public void setCallback(final Consumer<LinesReconstruction> callback) {
		this.callback = callback;
	}

	public Consumer<LinesReconstruction> getCallback() {
		return this.callback;
	}
}
