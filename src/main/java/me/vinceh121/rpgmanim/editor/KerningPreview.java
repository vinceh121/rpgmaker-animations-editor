package me.vinceh121.rpgmanim.editor;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import javax.swing.JComponent;

import me.vinceh121.rpgmanim.ConvertUtils;
import me.vinceh121.rpgmanim.cache.TextureCache;
import me.vinceh121.rpgmanim.entities.Kerning;

public class KerningPreview extends JComponent {
	private static final long serialVersionUID = 3358730012185701627L;
	private final Map<Character, Kerning> kernings;
	private String text;
	private int globalKerning;

	public KerningPreview(Map<Character, Kerning> kernings) {
		this.kernings = kernings;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		try {
			int offset = 0;

			for (char ch : this.text.toCharArray()) {
				Kerning kern = this.kernings.get(ch);

				String sheetName = Character.isUpperCase(ch) ? "Capital" : "Lowercase";
				BufferedImage sheet = TextureCache.getInstance().getCached(sheetName);

				Point p = ConvertUtils.getCoords(ConvertUtils.letterToPattern(ch));
				BufferedImage cellImg = sheet.getSubimage(p.x + kern.getyStart(), p.y, 192 - kern.getyStart(), 192);

				g.drawImage(cellImg, offset, 0, this);

				offset += (kern.getyEnd() - kern.getyStart()) + this.globalKerning;
			}

			this.setPreferredSize(new Dimension(offset, 192));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getGlobalKerning() {
		return globalKerning;
	}

	public void setGlobalKerning(int globalKerning) {
		this.globalKerning = globalKerning;
	}

	public Map<Character, Kerning> getKernings() {
		return kernings;
	}
}
