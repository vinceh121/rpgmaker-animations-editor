package me.vinceh121.rpgmanim.editor;

import java.awt.BorderLayout;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.Gutter;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import me.vinceh121.rpgmanim.entities.Animation;

public class AnimationEditor extends JFrame {
	private static final Logger LOG = LogManager.getLogger(AnimationEditor.class);
	private static final long serialVersionUID = 1375186216069065916L;
	private static final ObjectMapper mapper = new ObjectMapper();
	static {
		final DefaultIndenter indent = new DefaultIndenter("\t", DefaultIndenter.SYS_LF);
		final DefaultPrettyPrinter pp = new DefaultPrettyPrinter();
		pp.indentArraysWith(indent);
		pp.indentObjectsWith(indent);

		AnimationEditor.mapper.setDefaultPrettyPrinter(pp);
		AnimationEditor.mapper.enable(Feature.ALLOW_COMMENTS);
		// mapper.enable(JsonReadFeature.ALLOW_JAVA_COMMENTS);
		AnimationEditor.mapper.enable(SerializationFeature.INDENT_OUTPUT);
	}

	public AnimationEditor(final Animation animation) {
		this.setTitle("JSON Animation Editor: " + animation.getName());
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setSize(400, 500);
		this.setLayout(new BorderLayout());

		//// TEXT AREA
		final RSyntaxTextArea text = new RSyntaxTextArea();
		text.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSON_WITH_COMMENTS);
		text.setCodeFoldingEnabled(true);
		text.setTabsEmulated(false);
		text.setTabSize(4);
		text.setMarkOccurrences(true);
		text.setCaretPosition(0);
		text.requestFocusInWindow();
		try {
			Theme.load(AnimationEditor.class.getResourceAsStream("/org/fife/ui/rsyntaxtextarea/themes/monokai.xml"))
					.apply(text);
		} catch (final IOException e) {
			LOG.error("Failed to apply theme", e);
			JOptionPane.showMessageDialog(null, "Failed to apply theme: " + e);
		}
		try {
			text.setText(AnimationEditor.mapper.writeValueAsString(animation));
		} catch (final JsonProcessingException e) {
			LOG.error("Failed to serialize Animation", e);
			JOptionPane.showMessageDialog(null, "Failed to serialize Animation: " + e);
		}
		final RTextScrollPane scrollPane = new RTextScrollPane(text);
		scrollPane.getGutter().setBookmarkingEnabled(true);
		this.add(scrollPane, BorderLayout.CENTER);

		//// MENU
		final JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);

		final JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		final JMenuItem mntSave = new JMenuItem("Save");
		mntSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		mntSave.addActionListener(e -> {
			final Gutter gutter = scrollPane.getGutter();
			gutter.removeAllTrackingIcons();
			try {
				final Animation newAnim = AnimationEditor.mapper.readValue(text.getText(), Animation.class);
				newAnim.copyTo(animation);
			} catch (final JsonProcessingException e1) {
				e1.printStackTrace();
				text.setCaretPosition((int) e1.getLocation().getCharOffset() - 1);
				try {
					gutter.addLineTrackingIcon(e1.getLocation().getLineNr(),
							new ImageIcon(ImageIO.read(ClassLoader.getSystemResourceAsStream("error.png"))),
							e1.getMessage());
				} catch (final BadLocationException | IOException e2) {
					LOG.error("Failed to add gutter error icon thing", e2);
				}
			}
		});
		mnFile.add(mntSave);
	}
}
