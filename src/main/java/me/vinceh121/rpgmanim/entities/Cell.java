package me.vinceh121.rpgmanim.entities;

import java.util.ArrayList;

import me.vinceh121.rpgmanim.ConvertUtils;

public class Cell extends ArrayList<Double> {
	private static final long serialVersionUID = 3551279805050342934L;

	@Deprecated
	public Cell() {
	}

	public Cell(final boolean ensureContent) {
		if (ensureContent) {
			for (int i = 0; i < 7; i++) {
				this.add(0D);
			}
		}
	}

	public Cell(final Cell c) {
		super(c);
	}

	public void setPattern(final int pattern) {
		this.set(0, (double) pattern);
	}

	public int getPattern() {
		return this.get(0).intValue();
	}

	public void setX(final double pattern) {
		this.set(1, pattern);
	}

	public double getX() {
		return this.get(1);
	}

	public void setY(final double pattern) {
		this.set(2, pattern);
	}

	public double getY() {
		return this.get(2);
	}

	/**
	 * @param scale The scale between 0 and 100 inclusive
	 */
	public void setScale(final double scale) {
		this.set(3, scale);
	}

	/**
	 * @return The scale between 0 and 100 inclusive
	 */
	public double getScale() {
		return this.get(3);
	}

	public void setOpacity(final double opacity) {
		this.set(6, opacity);
	}

	public double getOpacity() {
		return this.get(6);
	}
	
	public void setBlendMode(final double blendMode) {
		this.set(7, blendMode);
	}

	public double getBlendMode() {
		return this.get(7);
	}

	/**
	 * Like equals, but ignores opacity and blend mode
	 * @param other
	 * @return
	 */
	public boolean fuzzyEquals(final Cell other) {
		return this.getPattern() == other.getPattern()
				&& this.getX() == other.getX()
				&& this.getY() == other.getY()
				&& this.getScale() == other.getScale();
	}

	@Override
	public String toString() {
		String letter;
		try {
			letter = Character.toString(ConvertUtils.patternToLetter((int) this.getPattern()));
		} catch (final IllegalArgumentException e) {
			letter = "<INVALID CHAR>";
		}
		return "Cell '"
				+ letter
				+ "' [getPattern()="
				+ this.getPattern()
				+ ", getX()="
				+ this.getX()
				+ ", getY()="
				+ this.getY()
				+ ", getScale()="
				+ this.getScale()
				+ ", getOpacity()="
				+ this.getOpacity()
				+ "]";
	}

}
