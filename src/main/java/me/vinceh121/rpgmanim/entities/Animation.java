package me.vinceh121.rpgmanim.entities;

import java.util.ArrayList;
import java.util.List;

public class Animation {
	private int id, position;
	private long animation1Hue, animation2Hue;
	private String animation1Name, animation2Name, name;
	private List<Timing> timings = new ArrayList<>();
	private List<Frame> frames = new ArrayList<>();

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(final int position) {
		this.position = position;
	}

	public long getAnimation1Hue() {
		return this.animation1Hue;
	}

	public void setAnimation1Hue(final long animation1Hue) {
		this.animation1Hue = animation1Hue;
	}

	public long getAnimation2Hue() {
		return this.animation2Hue;
	}

	public void setAnimation2Hue(final long animation2Hue) {
		this.animation2Hue = animation2Hue;
	}

	public String getAnimation1Name() {
		return this.animation1Name;
	}

	public void setAnimation1Name(final String animation1Name) {
		this.animation1Name = animation1Name;
	}

	public String getAnimation2Name() {
		return this.animation2Name;
	}

	public void setAnimation2Name(final String animation2Name) {
		this.animation2Name = animation2Name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<Timing> getTimings() {
		return this.timings;
	}

	public void setTimings(final List<Timing> timings) {
		this.timings = timings;
	}

	public List<Frame> getFrames() {
		return this.frames;
	}

	public void setFrames(final List<Frame> frames) {
		this.frames = frames;
	}

	public void copyTo(final Animation to) {
		to.setId(this.id);
		to.setPosition(this.position);
		to.setAnimation1Hue(this.animation1Hue);
		to.setAnimation1Name(this.animation1Name);
		to.setAnimation2Hue(this.animation2Hue);
		to.setAnimation2Name(this.animation2Name);
		to.setName(this.name);
		to.setTimings(this.timings);
		to.setFrames(this.frames);
	}
}
