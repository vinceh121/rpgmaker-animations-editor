package me.vinceh121.rpgmanim.entities;

import java.util.List;

public class Timing {
	private List<Integer> flashColor;
	private long flashDuration;
	private int flashScope, frame;
	private SoundEffect se;

	public List<Integer> getFlashColor() {
		return this.flashColor;
	}

	public void setFlashColor(final List<Integer> flashColor) {
		this.flashColor = flashColor;
	}

	public long getFlashDuration() {
		return this.flashDuration;
	}

	public void setFlashDuration(final long flashDuration) {
		this.flashDuration = flashDuration;
	}

	public int getFlashScope() {
		return this.flashScope;
	}

	public void setFlashScope(final int flashScope) {
		this.flashScope = flashScope;
	}

	public int getFrame() {
		return this.frame;
	}

	public void setFrame(final int frame) {
		this.frame = frame;
	}

	public SoundEffect getSe() {
		return this.se;
	}

	public void setSe(final SoundEffect se) {
		this.se = se;
	}

	public static class SoundEffect {
		private String name;
		private int pan, pitch, volume;

		public String getName() {
			return this.name;
		}

		public void setName(final String name) {
			this.name = name;
		}

		public int getPan() {
			return this.pan;
		}

		public void setPan(final int pan) {
			this.pan = pan;
		}

		public int getPitch() {
			return this.pitch;
		}

		public void setPitch(final int pitch) {
			this.pitch = pitch;
		}

		public int getVolume() {
			return this.volume;
		}

		public void setVolume(final int volume) {
			this.volume = volume;
		}

	}
}
