package me.vinceh121.rpgmanim.entities;

public class Kerning {
	private int yStart, yEnd;

	public Kerning() {
	}

	public Kerning(int yStart, int yEnd) {
		this.yStart = yStart;
		this.yEnd = yEnd;
	}

	public int getyStart() {
		return yStart;
	}

	public void setyStart(int yStart) {
		this.yStart = yStart;
	}

	public int getyEnd() {
		return yEnd;
	}

	public void setyEnd(int yEnd) {
		this.yEnd = yEnd;
	}

	@Override
	public String toString() {
		return "Kerning [yStart=" + yStart + ", yEnd=" + yEnd + "]";
	}
}
