package me.vinceh121.rpgmanim;

import java.awt.Point;
import java.util.List;

import me.vinceh121.rpgmanim.entities.Cell;

public class ConvertUtils {
	public static final int CELL_PIXEL_SIZE = 192;
	public static final char[][] CUSTOM_PATTERNS_CHARS = { { 126, 'é' } };

	public static Point getCoords(final int pattern) {
		final int sx = pattern % 5 * 192;
		final int sy = (int) Math.floor(pattern % 100 / 5) * 192;
		return new Point(sx, sy);
	}

	public static char patternToLetter(final int pattern) {
		for (final char[] ex : CUSTOM_PATTERNS_CHARS) {
			if (ex[0] == pattern) {
				return ex[1];
			}
		}

		if (pattern >= 100 && pattern <= 126) {
			return (char) (pattern - 3);
		} else if (pattern >= 0 && pattern <= 26) {
			return (char) (pattern + 65);
		} else {
			throw new IllegalArgumentException("Pattern out of range: " + pattern);
		}
	}

	public static int letterToPattern(final char letter) {
		for (final char[] ex : CUSTOM_PATTERNS_CHARS) {
			if (ex[1] == letter) {
				return ex[0];
			}
		}

		if (letter >= 97 && letter <= 122) { // lowercase
			return letter + 3;
		} else if (letter >= 65 && letter <= 90) { // uppercase
			return letter - 65;
		} else {
			throw new IllegalArgumentException("Character out of range: " + letter);
		}
	}

	public static String getText(final List<Cell> cells) {
		final StringBuilder sb = new StringBuilder();

		for (final Cell c : cells) {
			try {
				final char ch = ConvertUtils.patternToLetter(c.getPattern());
				sb.append(ch);
			} catch (final IllegalArgumentException e) {
				// fail safe
			}
		}

		return sb.toString();
	}
}
