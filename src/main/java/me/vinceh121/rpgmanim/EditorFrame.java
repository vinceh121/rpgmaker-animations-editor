package me.vinceh121.rpgmanim;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.sound.sampled.Clip;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.formdev.flatlaf.FlatDarculaLaf;

import me.vinceh121.rpgmanim.cache.AudioCache;
import me.vinceh121.rpgmanim.cache.TextureCache;
import me.vinceh121.rpgmanim.editor.AnimationEditor;
import me.vinceh121.rpgmanim.editor.KerningConfigurator;
import me.vinceh121.rpgmanim.editor.TextEditor;
import me.vinceh121.rpgmanim.entities.Animation;
import me.vinceh121.rpgmanim.entities.Animations;
import me.vinceh121.rpgmanim.entities.Cell;
import me.vinceh121.rpgmanim.entities.Frame;
import me.vinceh121.rpgmanim.entities.Timing;
import me.vinceh121.rpgmanim.entities.Timing.SoundEffect;

public class EditorFrame extends JFrame {
	private static final Logger LOG = LogManager.getLogger(EditorFrame.class);
	private static final long serialVersionUID = 2007343584968797187L;
	private static final ObjectMapper MAPPER = new ObjectMapper();
	private static final String CONFIG_PATH = "./config.json";
	private final Config config;
	private final FrameEditComponent frameEditPanel;
	private Animations animations;
	private JList<Animation> listAnims;
	private JSlider frameSlider;
	private boolean audio = false;

	public static void main(final String[] args) {
		LOG.info("Starting this shitcode ig");
		LOG.info("Build info:\n{}", BuildInfo.buildOptions());
		FlatDarculaLaf.setup();
		final EditorFrame ef = new EditorFrame();
		ef.setVisible(true);
	}

	public EditorFrame() {
		this.setTitle("RPGMaker Animations.json Editor");
		this.getContentPane().setLayout(new BorderLayout());
		this.setSize(600, 600);
		this.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		try {
			this.config = EditorFrame.MAPPER.readValue(new File(CONFIG_PATH), Config.class);

			TextureCache.getInstance().setAssetsRoot(this.config.getAssetsRoot());

			if (!this.config.getImages().isEmpty()) {
				TextureCache.getInstance().getAssetSources().putAll(this.config.getImages());
			}

			AudioCache.getInstance().setAssetsRoot(this.config.getAssetsRoot());

			if (!this.config.getAudios().isEmpty()) {
				AudioCache.getInstance().getAssetSources().putAll(this.config.getAudios());
			}
		} catch (final IOException e) {
			LOG.error("Error while loading config", e);
			JOptionPane.showMessageDialog(null, "Error while loading config: " + e);
			throw new RuntimeException(e);
		}

		this.buildMenu();

		this.frameEditPanel = new FrameEditComponent();
		this.add(this.frameEditPanel, BorderLayout.CENTER);

		this.listAnims = new JList<>();
		this.listAnims.setBounds(0, 0, 0, 989);
		this.listAnims.setCellRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = -8260637668489222277L;

			@Override
			public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
					final boolean isSelected, final boolean cellHasFocus) {
				final JLabel lbl = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected,
						cellHasFocus);
				final Animation an = (Animation) value;

				if (an == null) {
					lbl.setText("Null animation");
					return lbl;
				}

				if (an.getFrames().size() > 0) {
					lbl.setText(an.getId() + ": " + ConvertUtils.getText(an.getFrames().get(0)));
				} else {
					lbl.setText(an.getId() + ": <No Text>");
				}

				lbl.setToolTipText("<html>Name: " + an.getName() + "<br>Texture 1: " + an.getAnimation1Name()
						+ "<br>Texture 2: " + an.getAnimation2Name() + "</html>");
				return lbl;
			}
		});

		this.listAnims.addListSelectionListener(e -> {
			try {
				this.frameSlider.setValue(0);
				this.showFrame(e.getFirstIndex(), 0);
			} catch (final IOException e2) {
				e2.printStackTrace();
			}
		});
		this.getContentPane().add(new JScrollPane(this.listAnims), BorderLayout.WEST);

		this.frameSlider = new JSlider();
		this.frameSlider.setValue(0);
		this.frameSlider.setPaintTicks(true);
		this.frameSlider.setPaintLabels(true);
		this.frameSlider.setMinorTickSpacing(1);
		this.frameSlider.setMajorTickSpacing(5);
		this.frameSlider.addChangeListener(e -> {
			try {
				this.showFrame(this.listAnims.getSelectedIndex(), this.frameSlider.getValue());
			} catch (final IOException e1) {
				LOG.error("Failed to show frame", e1);
			}
		});
		this.getContentPane().add(this.frameSlider, BorderLayout.SOUTH);
	}

	public void saveConfig() {
		try {
			Files.writeString(Path.of(CONFIG_PATH),
					MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(this.config));
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e);
		}
	}

	private void showFrame(final int animId, final int frameId) throws MalformedURLException, IOException {
		final Animation anim = this.animations.get(animId);
		if (anim != null) {
			this.showFrame(anim, anim.getFrames().get(frameId), frameId);
		}
	}

	private void showFrame(final Animation anim, final Frame frame, final int frameId) // TODO this is awful and needs
																						// to change but i'm too lazy to
																						// make a custom jackson codec
			throws MalformedURLException, IOException {
		if (frame.isEmpty()) {
			LOG.warn("Current frame doesn't have any cells");
		}

		final BufferedImage img1, img2;

		this.frameSlider.setMaximum(anim.getFrames().size() - 1);
		if (anim.getAnimation1Name() != null && !anim.getAnimation1Name().isEmpty()) {
			img1 = TextureCache.getInstance().getCached(anim.getAnimation1Name());
		} else {
			img1 = null;
			if (!anim.getAnimation1Name().isEmpty()) { // bad logic, bad
				LOG.warn("No tileset defined for {}", anim.getAnimation1Name());
				JOptionPane.showMessageDialog(null, "No tileset defined for " + anim.getAnimation1Name());
			}
		}

		if (anim.getAnimation2Name() != null && !anim.getAnimation2Name().isEmpty()) {
			img2 = TextureCache.getInstance().getCached(anim.getAnimation2Name());
		} else {
			img2 = null;
			if (!anim.getAnimation2Name().isEmpty()) { // bad logic, bad
				LOG.warn("No tileset defined for {}", anim.getAnimation2Name());
				JOptionPane.showMessageDialog(null, "No tileset defined for " + anim.getAnimation2Name());
			}
		}

		this.frameEditPanel.setImg1(img1);
		this.frameEditPanel.setImg2(img2);
		this.frameEditPanel.setAnimation(anim);
		this.frameEditPanel.setFrame(frame);
		this.frameEditPanel.setAnimId(anim.getId());
		this.frameEditPanel.setFrameId(frameId);
		this.frameEditPanel.repaint();
	}

	private void buildMenu() {
		final JMenuBar bar = new JMenuBar();
		this.setJMenuBar(bar);

		final JMenu mnFile = new JMenu("File");
		bar.add(mnFile);

		final JMenuItem mntOpen = new JMenuItem("Open");
		mntOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		mntOpen.addActionListener(e -> {
			try {
				this.animations = EditorFrame.MAPPER.readValue(
						new FileReader(this.config.getAnimations(), Charset.forName(this.config.getCharset())),
						Animations.class);
				this.updateAnimList();
			} catch (final Exception e1) {
				LOG.error("Error while loading animations file", e1);
				JOptionPane.showMessageDialog(null, "Error while loading animations file: " + e1);
			}
		});
		mnFile.add(mntOpen);

		final JMenuItem mntSave = new JMenuItem("Save");
		mntSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		mntSave.addActionListener(e -> {
			try {
				EditorFrame.MAPPER.writeValue(
						new FileWriter(this.config.getAnimations(), Charset.forName(this.config.getCharset())),
						this.animations);
			} catch (final IOException e1) {
				LOG.error("Failed to save", e1);
				JOptionPane.showMessageDialog(null, "Failed to save: " + e1);
			}
		});
		mnFile.add(mntSave);

		final JMenuItem mntQuit = new JMenuItem("Quit");
		mntQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
		mntQuit.addActionListener(e -> System.exit(0));
		mnFile.add(mntQuit);

		final JMenu mnAnim = new JMenu("Animation");
		bar.add(mnAnim);

		final JMenuItem mntCreateAnim = new JMenuItem("Create animation");
		mntCreateAnim.addActionListener(e -> {
			final Animation anim = new Animation();
			anim.setId(this.animations.size());
			this.animations.add(anim);
			this.updateAnimList();
		});
		mnAnim.add(mntCreateAnim);

		final JMenuItem mntEditAnim = new JMenuItem("Edit animation");
		mntEditAnim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK));
		mntEditAnim.addActionListener(e -> {
			final Animation anim = this.listAnims.getSelectedValue();
			if (anim == null) {
				JOptionPane.showMessageDialog(null, "Select an animation baka");
				return;
			}
			final AnimationEditor edit = new AnimationEditor(anim);
			edit.setVisible(true);
		});
		mnAnim.add(mntEditAnim);

		final JMenuItem mntDeleteAnim = new JMenuItem("Delete animation(s)");
		mntDeleteAnim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		mntDeleteAnim.addActionListener(e -> {
			final List<Animation> anims = this.listAnims.getSelectedValuesList();

			final int result = JOptionPane.showConfirmDialog(null,
					String.format("Delete %d animation(s)? %s", anims.size(),
							anims.stream().map(Animation::getName).collect(Collectors.joining(", "))),
					"Deletion confirmation", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION) {
				this.animations.removeIf(anim -> anims.contains(anim));
				this.updateAnimList();
			}
		});
		mnAnim.add(mntDeleteAnim);

		final JMenuItem mntPlayAnim = new JMenuItem("Play animation");
		mntPlayAnim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
		mntPlayAnim.addActionListener(e -> {
			final long frameTimeGoal = 100;
			new Thread(() -> {
				final Animation anim = this.listAnims.getSelectedValue();
				for (int i = 0; i < anim.getFrames().size(); i++) {
					final long start = System.currentTimeMillis();
					if (audio) {
						for (final Timing tim : anim.getTimings()) {
							if (tim.getFrame() == i) {
								final SoundEffect se = tim.getSe();
								if (se != null) {
									try {
										final Clip clip = AudioCache.getInstance().getCached(se.getName());
										if (clip.isRunning())
											clip.stop();
										clip.setFramePosition(0);
										clip.start();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
							}
						}
					}
					try {
						this.showFrame(this.listAnims.getSelectedIndex(), i);
					} catch (final IOException e1) {
						JOptionPane.showMessageDialog(null, e1);
						e1.printStackTrace();
					}
					final long elapsed = System.currentTimeMillis() - start;
					if (elapsed < frameTimeGoal) {
						try {
							Thread.sleep(frameTimeGoal - elapsed);
						} catch (final InterruptedException e1) {
							e1.printStackTrace();
						}
					} else {
						LOG.warn("Couldn't meet frame rate: {} ms", elapsed);
					}
				}
			}, "PlayerThread").start();
		});
		mnAnim.add(mntPlayAnim);

		final JCheckBoxMenuItem mntcbAudio = new JCheckBoxMenuItem("Audio", this.audio);
		mntcbAudio.addActionListener(e -> this.audio = mntcbAudio.isSelected());
		mnAnim.add(mntcbAudio);

		final JMenuItem mntClearAnim = new JMenuItem("Clear animation");
		mntClearAnim.addActionListener(e -> {
			final int v = JOptionPane.showConfirmDialog(null,
					"Are you sure you wanna yeet the frames of that animation?", "Yeetus deletus?",
					JOptionPane.YES_NO_OPTION);

			if (v == JOptionPane.YES_OPTION) {
				this.animations.get(this.listAnims.getSelectedIndex()).setFrames(new ArrayList<>());
			}
		});
		mnAnim.add(mntClearAnim);
		
		final JMenuItem mntScale = new JMenuItem("Scale");
		mntScale.addActionListener(e -> {
			final double scale = Double.parseDouble(JOptionPane.showInputDialog("Scale:"));
			resizeAnimation(this.frameEditPanel.getAnimation(), scale);
		});
		mnAnim.add(mntScale);

		final JMenuItem mntFixAnimations = new JMenuItem("Fix animations");
		mntFixAnimations.addActionListener(e -> this.fixAnimations());
		mnAnim.add(mntFixAnimations);

		final JMenuItem mntOffset = new JMenuItem("Offset");
		mntOffset.addActionListener(e -> {
			final Animation anim = this.listAnims.getSelectedValue();

			if (anim == null) {
				JOptionPane.showMessageDialog(null, "Select an animation baka");
				return;
			}

			final OffsetDialog diag = new OffsetDialog(anim);
			diag.setOnUpdate(() -> {
				this.frameEditPanel.repaint();
			});
			diag.setVisible(true);
		});
		mnAnim.add(mntOffset);

		final JMenuItem mntCopy = new JMenuItem("Copy");
		mntCopy.addActionListener(e -> {
			final Animation selectedAnimation = this.listAnims.getSelectedValue();

			final ObjectNode node = MAPPER.valueToTree(selectedAnimation);
			final ObjectNode deepCopy = node.deepCopy();

			final Animation copy = MAPPER.convertValue(deepCopy, Animation.class);
			copy.setId(this.animations.size());
			this.animations.add(copy);
			this.updateAnimList();
		});
		mnAnim.add(mntCopy);
		
		mnAnim.addSeparator();
		
		final JMenuItem mntSpriteSheet = new JMenuItem("View sprite sheet 1");
		mntSpriteSheet.addActionListener(e -> {
			final Animation anim = this.listAnims.getSelectedValue();
			final BufferedImage img1;

			try {
				if (anim.getAnimation1Name() != null && !anim.getAnimation1Name().isEmpty()) {
					img1 = TextureCache.getInstance().getCached(anim.getAnimation1Name());
				} else {
					img1 = null;
					LOG.warn("No tileset defined for {}", anim.getAnimation1Name());
					JOptionPane.showMessageDialog(null, "No tileset defined for " + anim.getAnimation1Name());

					return;
				}

				SpriteSheetViewer viewer = new SpriteSheetViewer(img1);
				viewer.setVisible(true);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});
		mnAnim.add(mntSpriteSheet);

		final JMenu mnView = new JMenu("View");
		bar.add(mnView);

		final JCheckBoxMenuItem mntcbOpacity = new JCheckBoxMenuItem("Opacity", true);
		mntcbOpacity.addActionListener(e -> this.frameEditPanel.setRenderOpacity(mntcbOpacity.isSelected()));
		mnView.add(mntcbOpacity);

		final JMenuItem mntBackground = new JMenuItem("Background color");
		mntBackground.addActionListener(e -> this.frameEditPanel.setBackground(
				JColorChooser.showDialog(null, "Background title", this.frameEditPanel.getBackground(), false)));
		mnView.add(mntBackground);

		final JMenuItem mntViewScale = new JMenuItem("Scale: 1.0×");
		mntViewScale.addActionListener(e -> {
			this.frameEditPanel.setScale(Double.parseDouble(JOptionPane.showInputDialog("Inpüt ein scaeile: ")));
			mntViewScale.setText("Scale: " + this.frameEditPanel.getScale() + "×");
		});
		mnView.add(mntViewScale);

		final JMenu mnRecognition = new JMenu("Recognition");
		bar.add(mnRecognition);

		final JMenuItem mntTextRec = new JMenuItem("Text editor");
		mntTextRec.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));
		mntTextRec.addActionListener(e -> {
			final Animation anim = this.animations.get(this.listAnims.getSelectedIndex());
			final TextEditor te = new TextEditor(anim, this.config);
			te.setCallback(lr -> anim.setFrames(lr.getFrames()));
			te.setVisible(true);
		});
		mnRecognition.add(mntTextRec);

		final JMenuItem mntKerning = new JMenuItem("Configure kerning");
		mntKerning.addActionListener(e -> {
			final KerningConfigurator kern = new KerningConfigurator(this.config);
			kern.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					saveConfig();
				}
			});
			kern.setVisible(true);
		});
		mnRecognition.add(mntKerning);

		final JMenu mnCache = new JMenu("Cache");
		bar.add(mnCache);

		final JMenuItem mntClearCache = new JMenuItem("Clear cache");
		mntClearCache.addActionListener(e -> {
			TextureCache.getInstance().clear();
			AudioCache.getInstance().clear();
		});
		mnCache.add(mntClearCache);

		final JMenu mnHelp = new JMenu("Help");
		bar.add(mnHelp);

		final JMenuItem mntAbout = new JMenuItem("About");
		mntAbout.addActionListener(e -> {
			JOptionPane.showMessageDialog(null,
					"rpgmaker-animations-editor Copyright (C) 2020-present vinceh121" + "\n" + "\n"
							+ "This program is free software: you can redistribute it and/or modify\n"
							+ "it under the terms of the GNU General Public License as published by\n"
							+ "the Free Software Foundation, either version 3 of the License, or\n"
							+ "(at your option) any later version.\n"
							+ "This program is distributed in the hope that it will be useful,\n"
							+ "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
							+ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
							+ "GNU General Public License for more details.\n"
							+ "You should have received a copy of the GNU General Public License\n"
							+ "along with this program.  If not, see <http://www.gnu.org/licenses/>.\n" + "\n"
							+ "RPGMaker is a product of Gotcha Gotcha Games Inc.\n" + "\n" + "Astolfo is cute\n"
							+ "It's pronounced /ɡɪf/\n" + "\n" + "Build info:\n" + BuildInfo.buildOptions());
		});
		mnHelp.add(mntAbout);
	}

	private static void resizeAnimation(Animation anim, double scale) {
		for (Frame frame : anim.getFrames()) {
			for (Cell cell : frame) {
				cell.setX(cell.getX() * scale);
				cell.setY(cell.getY() * scale);
				cell.setScale(cell.getScale() * scale);
			}
		}
	}
	
	private void updateAnimList() {
		this.listAnims.setListData(new Vector<>(this.animations));
	}

	private void fixAnimations() {
		for (Animation animation : this.animations) {
			if (animation != null) {
				animation.getFrames().removeIf(Frame::isEmpty);

				for (Frame frame : animation.getFrames()) {
					for (Cell cell : frame) {
						if (cell.size() < 8) {
							for (int i = 0; i < 8 - cell.size(); i++) {
								cell.add(Double.valueOf(0));
							}
						}
					}
				}
			}
		}
	}
}
