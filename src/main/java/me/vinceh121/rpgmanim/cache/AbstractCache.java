package me.vinceh121.rpgmanim.cache;

import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractCache<T> {
	private final Logger LOG = LogManager.getLogger(getClass());
	private final Map<String, T> map = new Hashtable<>();
	private final Map<String, String> assetSources = new Hashtable<>();
	private String assetsRoot;
	private String assetFolder;
	private String extension;

	public boolean hasCached(final String name) {
		return this.map.containsKey(name);
	}

	public T getCached(final String name) throws IOException {
		final T img = this.map.get(name);
		if (img == null) {
			return this.load(name);
		}
		return img;
	}

	public void put(final String name, final T img) {
		this.map.put(name, img);
	}

	public T load(final String name) throws IOException {
		LOG.info("Loading source {}", name);
		final String path;
		if (this.assetSources.containsKey(name)) {
			path = this.assetSources.get(name);
		} else if (this.assetsRoot != null) {
			path = this.assetsRoot + this.assetFolder + name + this.extension;
		} else {
			throw new IOException("Couldn't load animation texture '"
					+ name
					+ "': it isn't defined and no assetRoot has been specified");
		}
		LOG.debug("Source {} is at {}", name, path);
		final T asset;
		try {
			asset = this.readAsset(new URL(path));
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
		this.put(name, asset);
		return asset;
	}

	public abstract T readAsset(final URL url) throws Exception;

	public void clear() {
		this.map.clear();
	}

	public String getAssetsRoot() {
		return assetsRoot;
	}

	public void setAssetsRoot(final String assetsRoot) {
		this.assetsRoot = assetsRoot;
	}

	public String getAssetFolder() {
		return assetFolder;
	}

	public void setAssetFolder(String assetFolder) {
		this.assetFolder = assetFolder;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Map<String, T> getMap() {
		return map;
	}

	public Map<String, String> getAssetSources() {
		return assetSources;
	}
}
