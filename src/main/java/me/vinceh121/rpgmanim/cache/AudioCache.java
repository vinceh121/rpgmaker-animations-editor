package me.vinceh121.rpgmanim.cache;

import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public final class AudioCache extends AbstractCache<Clip> {
	private static final AudioCache instance = new AudioCache();

	public AudioCache() {
		this.setAssetFolder("audio/se/");
		this.setExtension(".ogg");
	}

	@Override
	public Clip readAsset(final URL url) throws Exception {
		final AudioInputStream in = AudioSystem.getAudioInputStream(url);
		final AudioFormat baseFormat = in.getFormat();
		final AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(),
				16, baseFormat.getChannels(), baseFormat.getChannels() * 2, baseFormat.getSampleRate(), false);
		final AudioInputStream fin = AudioSystem.getAudioInputStream(decodedFormat, in);
		final Clip clip = AudioSystem.getClip();
		clip.open(fin);
		return clip;
	}

	public static AudioCache getInstance() {
		return AudioCache.instance;
	}
}
