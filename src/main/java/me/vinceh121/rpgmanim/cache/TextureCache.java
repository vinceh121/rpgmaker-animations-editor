package me.vinceh121.rpgmanim.cache;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public final class TextureCache extends AbstractCache<BufferedImage> {
	private static final TextureCache instance = new TextureCache();

	public TextureCache() {
		this.setAssetFolder("img/animations/");
		this.setExtension(".png");
	}

	@Override
	public BufferedImage readAsset(URL url) throws IOException {
		return ImageIO.read(url);
	}

	public static TextureCache getInstance() {
		return TextureCache.instance;
	}
}
