package me.vinceh121.rpgmanim;

import java.lang.reflect.Field;

public class BuildInfo {
	public static final String VERSION = "${project.version}", GIT_COMMIT = "${git.commit.id.abbrev}",
			GIT_COMMIT_DATE = "${git.commit.time}";

	public static String buildOptions() {
		final StringBuilder sb = new StringBuilder();
		for (final Field f : BuildInfo.class.getDeclaredFields()) {
			sb.append(f.getName());
			sb.append(" = ");
			try {
				sb.append(f.get(""));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
				sb.append(e.toString());
			}
			sb.append('\n');
		}
		return sb.toString();
	}
}
